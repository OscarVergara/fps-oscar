﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie1Behaviour : MonoBehaviour
{
    public enum State { Idle, Chase, Attack, Dead};
    public State state;

    private Laser player;
    private NavMeshAgent agent;
    private SoundPlayer sound;

    [Header("Zombie1 properties")]
    public int life = 5;

    [Header("Target Detection")]
    public float radius;
    public float idleRadius;
    public float chaseRadius;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;

    [Header("Patrol path")]
    public bool stopAtEachNode = true;
    public float timeStopped = 1.0f;
    private float timeCounter = 0.0f;
    public Transform[] pathNodes;
    private int currentNode = 0;
    private bool nearNode = false;

    [Header("Attack properties")]
    public float attackDistance;

    [Header("Animator")]
    public Animator animator;

    // Use this for initialization
    void Start ()
    {
        player = GameObject.Find("Player").GetComponent<Laser>();
        agent = GetComponent<NavMeshAgent>();
        sound = GetComponentInChildren<SoundPlayer>();

        nearNode = true;
        SetIdle();        
	}
	
	// Update is called once per frame
	void Update ()
    {
        switch(state)
        {
            case State.Idle:
                Idle();
                break;
            case State.Chase:
                Chase();
                break;
            case State.Dead:
                if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                {
                    Destroy(this.gameObject);
                }
                break;
            default:
                break;
        }
    }
    private void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if(cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;            
        }
        if (state == State.Attack && !targetDetected)
        {
            SetChase();  
        }
    }

    void Idle()
    {
        if(targetDetected)
        {
            SetChase();
            return;
        }
    }
    void Chase()
    {
        if(!targetDetected)
        {
            nearNode = true;
            SetIdle();
            return;
        }
        agent.SetDestination(targetTransform.position);

        if(Vector3.Distance(transform.position, targetTransform.position) <= attackDistance)
        {
            SetAttack();
        }


    }
    void Dead() { }

    void SetIdle()
    {
        agent.isStopped = true;        
        radius = idleRadius;
        timeCounter = 0;

        int random = Random.Range(0, 4);
        //sound.Play(random, 1);

        state = State.Idle;
        animator.SetTrigger("Attack");
        animator.SetBool("Walk", false);
        animator.SetBool("Idle", true);
    }
    void SetChase()
    {
        sound.Play(0, 1f);
        agent.isStopped = false;
        agent.SetDestination(targetTransform.position);
        agent.stoppingDistance = 2.0f;
        radius = chaseRadius;

        state = State.Chase;
        animator.SetBool("Idle", false);
        animator.SetTrigger("Attack");
        animator.SetBool("Walk", true);
    }
    void SetAttack()
    {
        sound.Play(1, 1f);
        agent.isStopped = true;
        radius = idleRadius;
        timeCounter = 0;

        int random = Random.Range(0, 4);
        //sound.Play(random, 1);

        state = State.Attack;
        animator.SetBool("Idle", false);
        animator.SetBool("Walk", false);
        animator.SetTrigger("Attack");
    }
    void SetDead()
    {
        //sound.Play(8, 1);
        animator.SetTrigger("Die");

        //Destroy(this.gameObject);
        state = State.Dead;
    }
    
    
    private void OnDrawGizmos()
    {
        Color color = Color.green;
        if(targetDetected) color = Color.red;
        color.a = 0.1f;
        Gizmos.color = color;
        Gizmos.DrawSphere(this.transform.position, radius);
    }

    public void Damage(int hit)
    {
        Debug.Log("Zombie1 damage");
        if(state == State.Dead) return;
        life -= hit;
        if(life <= 0) SetDead();
    }

	void OnCollisionEnter(Collision other) {
		if(other.gameObject.tag == "Shot") {
			Damage (5);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && state == State.Attack && animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.5f)
        {
            player.SetHealth(player.GetHealth() - 1);
            Debug.Log("AOEUBFSIEF");
        }
    }
}
