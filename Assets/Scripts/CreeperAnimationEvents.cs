﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreeperAnimationEvents : MonoBehaviour {

    CreeperBehaviour creeper;
    private void Start()
    {
        creeper = GetComponent<CreeperBehaviour>();
    }
    public void EndExplosion()
    {
        Debug.Log("Explosion!!!");
        creeper.Explosion();
    }
}
