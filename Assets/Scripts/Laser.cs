﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Laser : MonoBehaviour {

	public LayerMask mask;

    private int health;
    public Slider healthSlider;
	public bool isShooting;
	public bool isReloading;
	public float reloadTime;

	public AudioClip[] fx;
	private AudioSource audioSource;
    [System.Serializable]
    public class Weapon
    {
        public string name;
        public int maxAmmo;
        public int clipSize;
        public int clipAmmo;
        public int currentAmmo;
        public float fireRate;
        public float hitForce;
        public float hitDamage;
        public float maxDistance;
        public GameObject model;
        public Animator animator;
    }

    public Weapon[] weapons;
    public Weapon currentWeapon;
    private Vector3 impactPosition;

	private void Start()
	{
		audioSource = GetComponent<AudioSource>();
		isShooting = false;
		isReloading = false;
		currentWeapon.currentAmmo = currentWeapon.maxAmmo - currentWeapon.clipSize;
        currentWeapon = weapons[1];
        health = 10;
        healthSlider.maxValue = health;
        healthSlider.normalizedValue = healthSlider.maxValue;
        Debug.Log(health);

    }
    private void Update()
    {
        Debug.Log(health);
        healthSlider.value = health;
    }
    public void Shot()
	{
        if (isShooting || isReloading || (currentWeapon.clipAmmo == 0 && currentWeapon.currentAmmo == 0)) return;
        if (currentWeapon.clipAmmo == 0 && currentWeapon.currentAmmo > 0)
        {
            Reload();
            return;
        }

        PlayShootAnim();

        PlaySFX(0, 0.25f, true);

		isShooting = true;
        currentWeapon.clipAmmo--;
        List<Ray> rays = new List<Ray>();
        switch (currentWeapon.name)
        {
            case "Pistol":
                rays.Add(Camera.main.ScreenPointToRay(Input.mousePosition));
                break;
            case "Shotgun":
                rays.Add(Camera.main.ScreenPointToRay(Input.mousePosition));
                rays.Add(Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x - 150, Input.mousePosition.y, Input.mousePosition.z)));
                rays.Add(Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x - 300, Input.mousePosition.y, Input.mousePosition.z)));
                rays.Add(Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x + 150, Input.mousePosition.y, Input.mousePosition.z)));
                rays.Add(Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x + 300, Input.mousePosition.y, Input.mousePosition.z)));
                break;
        }
		RaycastHit hit;
        for (int i = 0; i < rays.Count; i++)
        {
            Debug.DrawRay(this.transform.position, rays[i].direction, Color.red, 10);
            if (Physics.Raycast(rays[i], out hit, Mathf.Infinity, mask, QueryTriggerInteraction.Collide))
            {
                if (hit.rigidbody != null)
                {
                    //hit.rigidbody.AddForce(rays[i].direction * currentWeapon.hitForce, ForceMode.Impulse);
                    if (hit.transform.gameObject.CompareTag("Enemy"))
                    {
                        Debug.LogError("ZOMBIE DAMAGE");
                    }
                }

                hit.transform.gameObject.SendMessage("Damage", 1);

                impactPosition = hit.point;
                if (GameObject.Find("Shot Impact") == null)
                {
                    GameObject impact = new GameObject("Shot Impact");
                    impact.transform.position = impactPosition;
                    AudioSource audioImpact = impact.AddComponent<AudioSource>();
                    audioImpact.clip = fx[1];
                    audioImpact.volume = 0.15f;
                    audioImpact.Play();
                    Destroy(impact, fx[1].length);
                }
            }
        }

		StartCoroutine(WaitFireRate());
	}
	private IEnumerator WaitFireRate()
	{	Debug.Log("Empieza la corutina");
		yield return new WaitForSeconds(currentWeapon.fireRate);
		isShooting = false;
		Debug.Log("Termina la corutina");
	}

	public void Reload()
	{
        if (isReloading || currentWeapon.clipAmmo == currentWeapon.clipSize || currentWeapon.currentAmmo == 0) return;
		isReloading = true;
        PlayReloadAnim();
        PlaySFX(2, 0.25f, false);
		StartCoroutine(WaitForReload());
    }
    private IEnumerator WaitForReload()
    {
        yield return new WaitForSeconds(reloadTime);

        currentWeapon.currentAmmo -= currentWeapon.clipSize - currentWeapon.clipAmmo;
        currentWeapon.clipAmmo = currentWeapon.clipSize;
        isReloading = false;
    }
    public void SwitchWeapon(int _weapon)
    {
        currentWeapon = weapons[_weapon];
        for(int i = 0; i < weapons.Length; i++)
        {
            if (i == _weapon)
                weapons[i].model.SetActive(true);
            else
                weapons[i].model.SetActive(false);
        }
    }
    public void AmmoPickup(int _weaponID)
    {
        if ((weapons[_weaponID].currentAmmo + weapons[_weaponID].clipAmmo) != weapons[_weaponID].maxAmmo)
        {
            weapons[_weaponID].currentAmmo = weapons[_weaponID].maxAmmo - weapons[_weaponID].clipAmmo;
        }
        if (weapons[_weaponID].currentAmmo > 100)
            weapons[_weaponID].currentAmmo = 100;
    }
    void PlaySFX(int clip, float volume, bool randomPitch)
	{
		audioSource.clip = fx[clip];
		audioSource.volume = volume;

		if(randomPitch)
		{
			audioSource.pitch = Random.Range(0.95f, 1.05f);
		}

		audioSource.Play();
	}

	private void OnDrawGizmos()
	{
		if(isShooting)
		{
			Color color = Color.red;
			color.a = 0.4f;
			Gizmos.color = color;
			Gizmos.DrawSphere(impactPosition, 0.35f);
		}
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PistolAmmo" && weapons[0].clipAmmo + weapons[0].currentAmmo != weapons[0].maxAmmo) 
        {
            AmmoPickup(0);
            Destroy(other.gameObject);
        }
        if (other.tag == "ShotgunAmmo" && weapons[1].clipAmmo + weapons[1].currentAmmo != weapons[1].maxAmmo)
        {
            AmmoPickup(1);
            Destroy(other.gameObject);
        }
    }
    private void PlayShootAnim()
    {
        currentWeapon.animator.SetTrigger("Shoot");
    }
    private void PlayReloadAnim()
    {
        currentWeapon.animator.SetTrigger("Reload");
    }
    public int GetHealth()
    {
        return health;
    }
    public void SetHealth(int _health)
    {
        health = _health;
        if (health <= 0)
            SceneManager.LoadScene(0);
    }
}
