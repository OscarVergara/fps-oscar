﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour
{
    GameObject player;
    PlayerMovement playerController;
    Laser laser;
    [SerializeField] TextMeshProUGUI ammoText;
    [SerializeField] Slider ammoSlider;
	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        laser = playerController.GetComponent<Laser>();
        ammoSlider.maxValue = laser.currentWeapon.clipSize;
	}
	
	// Update is called once per frame
	void Update () {
        ammoText.text = laser.currentWeapon.clipAmmo + "/" + laser.currentWeapon.currentAmmo;
        ammoSlider.value = laser.currentWeapon.clipAmmo;
    }
}
