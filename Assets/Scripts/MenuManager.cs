﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {
    EventSystem es;
    public void Start()
    {
        es = EventSystem.current;
        es.SetSelectedGameObject(GameObject.Find("Play"));
    }
    
    public void GoToGame()
    {
        SceneManager.LoadScene(sceneName:"Prototype");
    }
    public void Exit()
    {
        Application.Quit();
    }

}
