﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private Laser player;
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Laser>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && GetComponent<ParticleSystem>().isPlaying)
        {
            player.SetHealth(player.GetHealth() - 1);
            Debug.Log("AOEUBFSIEF");
        }
    }
}
